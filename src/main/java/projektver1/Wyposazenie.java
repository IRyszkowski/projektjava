/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projektver1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

/**
 *
 * @author Iwo Ryszkowski
 */
public class Wyposazenie extends Przedmiot{
   int gdzie;
   int pancerz;
   int zmianaAtrubutow[];
   int wymSila;
   int wymZrecznosc;
   int wymSzybkosc;
   int wymInteligencja;
   int wymWytrzymalosc;
   int wymSilaWoli;
   
   
   Wyposazenie(String plik) throws FileNotFoundException
   {
       super(plik);
       zmianaAtrubutow = new int[8];
       Scanner odczyt = new Scanner(new BufferedReader(new FileReader("przedmioty\\" + plik + ".txt")));
       while(!odczyt.nextLine().equals("GDZIE:"));
       gdzie = Integer.valueOf(odczyt.nextLine());
       while(!odczyt.nextLine().equals("PANCERZ:"));
       pancerz = Integer.valueOf(odczyt.nextLine());
       while(!odczyt.nextLine().equals("ZMIANA:"));
       for(int i = 0;i < 8;i++)
       {
            zmianaAtrubutow[i] = Integer.valueOf(odczyt.nextLine());
       }
       while(!odczyt.nextLine().equals("WYMAGANIA:"));
       wymSila = Integer.valueOf(odczyt.nextLine());
       wymZrecznosc = Integer.valueOf(odczyt.nextLine());
       wymSzybkosc = Integer.valueOf(odczyt.nextLine());
       wymInteligencja = Integer.valueOf(odczyt.nextLine());
       wymWytrzymalosc = Integer.valueOf(odczyt.nextLine());
       wymSilaWoli = Integer.valueOf(odczyt.nextLine());
       odczyt.close();
   }
   Wyposazenie()
   {
       super();
       gdzie = -1;
       pancerz = 0;
       zmianaAtrubutow = new int[2];
       zmianaAtrubutow[0] = -1;
       zmianaAtrubutow[1] = -1;
   }
}
