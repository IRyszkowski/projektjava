/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projektver1;

import java.util.Random;

/**
 *
 * @author Iwo Ryszkowski
 */
public class Testy {

    /**
     *
     * @param stat1
     * @param stat2
     * @param szczescie
     * @return
     */
    public static int test1(int stat1,int stat2, int szczescie)
    {
        Random randomGenerator = new Random();
        int sukcesy = 0;
        int trudnosc = stat1 - stat2;
        if(trudnosc <= 0)
        {
            trudnosc = 1;
        }
        for(int i =0;i < trudnosc;i++)
        {
            int rzut = randomGenerator.nextInt(6) + szczescie;
            if(rzut >= 4)
            {
                sukcesy++;
            }
        }
        return sukcesy;
    }
    
    /**
     *
     * @param bound
     * @return
     */
    public static int test2(int bound)
    {
        Random randomGenerator = new Random();
        return randomGenerator.nextInt(bound);
    }
}
