/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projektver1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

/**
 *
 * @author Iwo Ryszkowski
 */
public class Atak {
    String nazwa;
    int obrazeniaModyfikator;
    int trafienieModyfikator;
    int koszt;
    int stany[];
    
    /**
     *
     * @param plik
     * @throws FileNotFoundException
     */
    public Atak(String plik) throws FileNotFoundException
    {
        Scanner odczyt = new Scanner(new BufferedReader(new FileReader("ataki\\" + plik + ".txt")));
        while(!odczyt.nextLine().equals("NAZWA:"));
        nazwa = odczyt.nextLine();
        while(!odczyt.nextLine().equals("OBRAZENIA:"));
        obrazeniaModyfikator = Integer.valueOf(odczyt.nextLine());
        while(!odczyt.nextLine().equals("TRAFIENIE:"));
        trafienieModyfikator = Integer.valueOf(odczyt.nextLine());
        while(!odczyt.nextLine().equals("KOSZT:"));
        koszt = Integer.valueOf(odczyt.nextLine());
        stany = new int[3];
        while(!odczyt.nextLine().equals("STANY:"));
        stany[0] = Integer.valueOf(odczyt.nextLine());
        stany[1] = Integer.valueOf(odczyt.nextLine());
        stany[2] = Integer.valueOf(odczyt.nextLine());
        odczyt.close();
    }
    
    /**
     *
     */
    public Atak()
    {
        nazwa = "Uderzenie z piesci";
        koszt = 1;
        obrazeniaModyfikator = 0;
        trafienieModyfikator = 0;
        stany = new int[3];
        stany[0] = -1;
        stany[1] = -1;
        stany[2] = -1;
    }
    
    /**
     *
     * @return
     */
    public String opisAtaku()
    {
        String opis = nazwa + " ";
        opis += "MT: " + trafienieModyfikator + " ";
        opis += "MO: " + obrazeniaModyfikator + " ";
        opis += "Koszt: " + koszt + " ";
        if(stany[0] != -1)
        {
            opis += "szansa na ";
            switch(stany[0])
            {
                case 0:
                   opis +=  "obnizenie sily o " + stany[1] + "punktow na " + stany[2] + "tur.";
                   break;
                case 1:
                   opis +=  "obnizenie zrecznosci o " + stany[1] + "punktow na " + stany[2] + "tur.";
                   break;
                case 2:
                   opis +=  "obnizenie szubkosci o " + stany[1] + "punktow na " + stany[2] + "tur.";
                   break;
                case 3:
                   opis +=  "obnizenie wytrzymalosci o " + stany[1] + "punktow na " + stany[2] + "tur.";
                   break;
                case 4:
                   opis +=  "obnizenie inteligecji o " + stany[1] + "punktow na " + stany[2] + "tur.";
                   break;
                case 5:
                   opis +=  "obnizenie sily woli o " + stany[1] + "punktow na " + stany[2] + "tur.";
                   break;
                case 6:
                   opis +=  "ogluszenie na " + stany[2] + "tur.";
                   break;
                default:
                   opis +=  "nic";
                   break;
            }
        }
        return opis;
    }
}
