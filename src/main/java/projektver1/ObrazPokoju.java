/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projektver1;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.image.BufferedImage;
import static java.awt.image.BufferedImage.TYPE_INT_ARGB;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 *
 * @author Iwo Ryszkowski
 */
public class ObrazPokoju extends JComponent{
    BufferedImage pokoj;
    BufferedImage korytaz0;
    BufferedImage korytaz1;
    BufferedImage korytaz2;
    BufferedImage korytaz3;
    int korytaze;
    
    /**
     *
     */
    public ObrazPokoju()
    {
        super();
        try {
		pokoj = ImageIO.read(new File("mapapng\\pusty.png"));
	} catch (IOException e) {
		System.err.println("Blad odczytu obrazka");
		e.printStackTrace();
	}
        setPreferredSize(new Dimension(pokoj.getWidth(), pokoj.getHeight()));
    }
    
    /**
     *
     * @param pokojP
     */
    public void odslon(Pomieszczenie pokojP)
    {
        korytaze = 0;
	try {
		pokoj = ImageIO.read(new File("mapapng\\pokoj.png"));
	} catch (IOException e) {
		System.err.println("Blad odczytu obrazka");
		e.printStackTrace();
	}
        if(pokojP.drzwi[0])
        {
            korytaze += 1;
        }
        if(pokojP.drzwi[1])
        {
            korytaze += 2;
        }
        if(pokojP.drzwi[2])
        {
            korytaze += 4;
        }
        if(pokojP.drzwi[3])
        {
            korytaze += 8;
        }
        
        if(korytaze >= 8)
        {
            try {
		korytaz3 = ImageIO.read(new File("mapapng\\korytaz3.png"));
            } catch (IOException e) {
                    System.err.println("Blad odczytu obrazka");
                    e.printStackTrace();
            }
            korytaze -= 8;
        }
        if(korytaze >= 4)
        {
            try {
		korytaz2 = ImageIO.read(new File("mapapng\\korytaz2.png"));
            } catch (IOException e) {
                    System.err.println("Blad odczytu obrazka");
                    e.printStackTrace();
            }
            korytaze -= 4;
        }
        if(korytaze >= 2)
        {
            try {
		korytaz1 = ImageIO.read(new File("mapapng\\korytaz1.png"));
            } catch (IOException e) {
                    System.err.println("Blad odczytu obrazka");
                    e.printStackTrace();
            }
            korytaze -= 2;
        }
        if(korytaze >= 1)
        {
            try {
		korytaz0 = ImageIO.read(new File("mapapng\\korytaz0.png"));
            } catch (IOException e) {
                    System.err.println("Blad odczytu obrazka");
                    e.printStackTrace();
            }
            korytaze -= 8;
        }
    }
    
    @Override
    public void paintComponent(Graphics g) {
	Graphics2D g2d = (Graphics2D) g;
	g2d.drawImage(pokoj, 0, 0, this);
        g2d.drawImage(korytaz0, 0, 0, this);
        g2d.drawImage(korytaz1, 0, 0, this);
        g2d.drawImage(korytaz2, 0, 0, this);
        g2d.drawImage(korytaz3, 0, 0, this);
    }
}
