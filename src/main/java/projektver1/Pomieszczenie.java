/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projektver1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;
import java.util.Vector;

/**
 *
 * @author Iwo Ryszkowski
 */
public class Pomieszczenie {
    String nazwa;
    private Mebel obiekty[];
    private Kontener kontenery[];
    boolean drzwi[];
    String wyglad;
    boolean odkryty;
    
    Pomieszczenie()
    {
        nazwa = "null";
    }
    
    Pomieszczenie(String plik) throws FileNotFoundException
    {
        odkryty = false;
        drzwi = new boolean[4];
        Scanner odczyt = new Scanner(new BufferedReader(new FileReader("pomieszczenia\\" + plik + ".txt")));
        while(!odczyt.nextLine().equals("NAZWA:"));
        nazwa = odczyt.nextLine();
        while(!odczyt.nextLine().equals("OBIEKTY:"));
        String ile = odczyt.nextLine();
        if(!ile.equals("end"))
        {
            obiekty = new Mebel[Integer.valueOf(ile)];
            String tmp;
            for(int i = 0;i < Integer.valueOf(ile);i++)
            {
                tmp = odczyt.nextLine();
                obiekty[i] = new Mebel(Integer.valueOf(tmp));
            }
        }
        else
        {
            obiekty = new Mebel[1];
            obiekty[0] = new Mebel();
        }
        while(!odczyt.nextLine().equals("KONTENERY:"));
        ile = odczyt.nextLine();
        if(!ile.equals("end"))
        {
            kontenery = new Kontener[Integer.valueOf(ile)];
            String tmp;
            for(int i = 0;i < Integer.valueOf(ile);i++)
            {
                tmp = odczyt.nextLine();
                kontenery[i] = new Kontener(Integer.valueOf(tmp));
            }
        }
        else
        {
            kontenery = new Kontener[1];
            kontenery[0] = new Kontener();
        }
        while(!odczyt.nextLine().equals("OPIS:"));
        wyglad = odczyt.nextLine() + " Znajdują się w nim: ";
        if(obiekty[0].numer > 0)
        {
            for(int i = 0;i < obiekty.length ;i++)
            {
                wyglad = wyglad + obiekty[i].podajNazwe() + ", ";
            }
        }
        if(kontenery[0].numer > 0)
        {
            for(int i = 0;i < kontenery.length ;i++)
            {
                wyglad = wyglad + kontenery[i].podajNazwe() + ", ";
            }
        }
        wyglad +=". Drzwi wychodzą na";
        while(!odczyt.nextLine().equals("DRZWI:"));
        int drz = Integer.valueOf(odczyt.nextLine());
        if(drz >= 8)
        {
            drzwi[3] = true;
            drz -= 8;
        }
        else
        {
            drzwi[3] = false;
        }
        if(drz >= 4)
        {
            drzwi[2] = true;
            drz -= 4;
        }
        else
        {
            drzwi[2] = false;
        }
        if(drz >= 2)
        {
            drzwi[1] = true;
            drz -= 2;
        }
        else
        {
            drzwi[1] = false;
        }
        if(drz == 1)
        {
            drzwi[0] = true;
        }
        else
        {
            drzwi[0] = false;
        }
        byte tmp2 = 0;
        if(drzwi[0] == true)
        {
            tmp2 = 1;
            wyglad +=" północ";
        }
        if(drzwi[1] == true)
        {
            if(tmp2 == 1)
            {
                wyglad +=",";
            }
            else
            {
                tmp2 = 1;   
            }
            wyglad +=" wschód";
        }
        if(drzwi[2] == true)
        {
            if(tmp2 == 1)
            {
                wyglad +=",";
            }
            else
            {
                tmp2 = 1;   
            }
            tmp2 = 1;
            wyglad +=" południe";
        }
        if(drzwi[3] == true)
        {
            if(tmp2 == 1)
            {
                wyglad +=",";
            }
            else
            {
                tmp2 = 1;   
            }
            tmp2 = 1;
            wyglad +=" zachód";
        }
         wyglad +=".";
        odczyt.close();
    }
    
    String opisz()
    {
        return wyglad;
    }
    
    /**
     *
     */
    public void odkryjPomieszczenie()
    {
        odkryty = true;
    }
    
    /**
     *
     * @return
     */
    public int ilosc()
    {
        return obiekty.length;
    }
    
    /**
     *
     * @param ktory
     * @return
     */
    public String opiszObiekt(int ktory)
    {
        return obiekty[ktory].podajOpis();
    }
    
    /**
     *
     * @param ktory
     * @return
     */
    public String opiszKontener(int ktory)
    {
        return kontenery[ktory].podajOpis();
    }
    
    /**
     *
     * @param ktory
     * @param g
     * @return
     */
    public String sprawdzKontener(int ktory, OsobaGracz g)
    {
        String opis = "";
        if(ktory >= kontenery.length)
        {
            return "blad!";
        }
        Kontener k = kontenery[ktory];
        if(k.czyOtwarta() == true)
        {
            opis = k.nazwa + "jest otwart";
            if(k.nazwa.endsWith("a"))
            {
                return opis + "a";
            }
            else
            {
                return opis + "y";
            }
        }
        else if(k.czyRozbrojona() == false)
        {
            int czy = k.wykryjPulapke(g);
            switch(czy)
            {
                case -1:
                    return k.dzialaniePulapki(g);
                case 0:
                    break;
                case 1:
                    return "Znalasles pulapke!";
                default:
                    return "Blad!";
            }
        }
        opis = k.nazwa + "wydaje się być pozbawion";
        if(k.nazwa.endsWith("a"))
        {
            opis += "a";
        }
        else
        {
            opis += "y";
        }
        return opis + "pulapek";
    }
    
    /**
     *
     * @param nazwa
     * @return
     */
    public Mebel przeszukaPomieszczenie(String nazwa)
    {
        if(obiekty[0].numer != -1)
        {
            for(int i = 0;i < obiekty.length;i++)
            {
                if(obiekty[i].podajNazwe().equals(nazwa))
                {
                    return obiekty[i];
                }
            }
        }
        if(kontenery[0].numer != -1)
        {
            for(int i = 0;i < kontenery.length;i++)
            {
                if(kontenery[i].podajNazwe().equals(nazwa))
                {
                    return kontenery[i];
                }
            }
        }
        return new Mebel();
    }
    
    /**
     *
     * @param nazwa
     * @return
     */
    public Przedmiot znajdzWKontenerach(String nazwa)
    {
        Przedmiot p = new Przedmiot();
        for(int i = 0;i < kontenery.length;i++)
        {
            Kontener k = kontenery[i];
            if(k.czyOtwarta())
            {
                p = k.znajdzPrzedmiot(nazwa);
                if(p.numer != -1)
                {
                    break;
                }
            }
        }
        return p;
    }
    
    /**
     *
     * @param komenda
     * @param g
     * @return
     */
    public String uzyjObiekt(String komenda, OsobaGracz g)
    {
        if(obiekty.length > 0)
        {
            for(int i = 0;i < obiekty.length;i++)
            {
                if(obiekty[i].numer == -1)
                {
                    return "null";
                }
                if(obiekty[i].nazwa.equals(komenda) == true)
                {
                    if(obiekty[i].uzyte == true)
                    {
                        return "Juz uzyles " + komenda + ".\n";
                    }
                    String ret = obiekty[i].uzyj(g);
                    return ret;
                }
            }
        }
        return "null";
    }
    
    /**
     *
     * @param nazwa
     */
    public void usunPrzedmiotPomieszczenie(String nazwa)
    {
        for(int i = 0;i < kontenery.length;i++)
        {
            if(kontenery[i].usunPrzedmiot(nazwa))
            {
                break;
            }
        }
    }
    
    /**
     *
     * @param nazwa
     */
    public void kontenerOtwartyPomieszczenie(String nazwa)
    {
        for(int i = 0;i < kontenery.length;i++)
        {
            if(kontenery[i].nazwa.equals(nazwa))
            {
                kontenery[i].otworz();
                break;
            }
        }
    }
}
