/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projektver1;

import java.awt.Dimension;
import java.awt.Graphics;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;
import java.util.Vector;
import javax.swing.JPanel;

/**
 *
 * @author Iwo Ryszkowski
 */
public class Mapa{
    Pomieszczenie plansza[][];
    Osoba npc[];

    /**
     *
     * @param nazwa
     * @throws FileNotFoundException
     */
    public Mapa(String nazwa) throws FileNotFoundException
    {
        Scanner odczyt = new Scanner(new BufferedReader(new FileReader(new File("mapa\\" + nazwa +".txt"))));
        int x,y;
        while(!odczyt.nextLine().equals("WYMIARY:"));
        x = Integer.valueOf(odczyt.nextLine());
        y = Integer.valueOf(odczyt.nextLine());
        while(!odczyt.nextLine().equals("POMIESZCZENIA:"));
        plansza = new Pomieszczenie[y][x];
        for(int i = 0;i < y;i++)
        {
            for(int j = 0;j < x;j++)
            {
                String nr = Integer.toString(odczyt.nextInt());
                if(nr.equals("0"))
                {
                    plansza[i][j] = new Pomieszczenie();
                }
                else
                {
                    plansza[i][j] = new Pomieszczenie(nr);
                }
            }
        }
        while(!odczyt.nextLine().equals("NPC:"));
        int ile = Integer.valueOf(odczyt.nextLine());
        npc = new Osoba[ile];
        String nextNPC;
        int gdzieNPC;
        for(int i = 0;i < ile;i++)
        {
            nextNPC = odczyt.nextLine();
            gdzieNPC = Integer.valueOf(odczyt.nextLine());
            if(nextNPC.startsWith("WRG"))
            {
                npc[i] = new OsobaPotwor(nextNPC.replaceFirst("WRG", "potwory\\\\"),i,gdzieNPC);
            }
            else
            {
                npc[i] = new Osoba(nextNPC,i,gdzieNPC);
            }
        }
    }
    
    /**
     *
     * @param numer
     * @return
     */
    public int[] wspolzedne(int numer)
    {
        int x,y;
        y=numer/plansza[0].length;
        if(numer%plansza[0].length == 0)
        {
            y--;
        }
        x = numer - plansza[0].length*y - 1;
        int tab[] = {y,x};
        return tab;
    }
    
    /**
     *
     * @param wsp
     * @return
     */
    public int numer(int wsp[])
    {
        return wsp[0]*plansza[0].length + wsp[1] + 1;
    }

    /**
     *
     * @param nr
     * @param kierunek
     * @return
     */
    public int ruch(int nr, int kierunek)
    {
        if(kierunek == -1)
        {
            return -1;
        }
        int tab[] = wspolzedne(nr);
        if(plansza[tab[0]][tab[1]].drzwi[kierunek] == true)
        {
            switch(kierunek)
            {
                case 0:
                    tab[0]--;
                    break;
                case 1:
                    tab[1]++;
                    break;
                case 2:
                    tab[0]++;
                    break;
                case 3:
                    tab[1]--;
                    break;
                default:
                    break;
            }
            return tab[0]*plansza[0].length + tab[1] + 1;
        }
        odkryjPokoj(tab);
        return 0;
    }
    
    /**
     *
     * @param numer
     * @param ktory
     * @return
     */
    public String rozejzyjSie(int numer, int ktory)
    {
        int wsp[] = wspolzedne(numer);
        String opis = "";
        switch(ktory)
        {
            case 0:
                break;
            case 1:
                if(plansza[wsp[0]][wsp[1]].drzwi[0])
                {
                    opis = "Na polnocy widzisz ";
                    wsp[0]--;
                }
                else
                {
                    opis = "Na polnoc nie wychodza drzwi";
                    return opis;
                }
                break;
            case 2:
                if(plansza[wsp[0]][wsp[1]].drzwi[1])
                {
                    opis = "Na wschodzie widzisz ";
                    wsp[1]++;
                }
                else
                {
                    opis = "Na wschod nie wychodza drzwi";
                    return opis;
                }
                break;
            case 3:
                if(plansza[wsp[0]][wsp[1]].drzwi[2])
                {
                    opis = "Na poludniu widzisz ";
                    wsp[0]++;
                }
                else
                {
                    opis = "Na poludnie nie wychodza drzwi";
                    return opis;
                }
                break;
            case 4:
                if(plansza[wsp[0]][wsp[1]].drzwi[3])
                {
                    opis = "Na zachodzie widzisz ";
                    wsp[1]--;
                }
                else
                {
                    opis = "Na zachod nie wychodza drzwi";
                    return opis;
                }
                break;
            
        }
        opis += plansza[wsp[0]][wsp[1]].opisz();
        opis += "\nW pomieszczeniu znajduje sie: ";
        numer = numer(wsp);
        for(int i = 0;i < npc.length;i++)
        {
            if(npc[i].podajPolozenie() == numer)
            {
                opis += (npc[i].nazwa + " ");
            }
        }
        if(opis.endsWith("W pomieszczeniu znajduje sie: "))
        {
            if(opis.startsWith("Na"))
            {
                opis = opis.replaceFirst("W pomieszczeniu znajduje sie: ", "W pomieszczeniu nikogo nie ma.");
            }
            else
            {
                opis = opis.replaceFirst("W pomieszczeniu znajduje sie: ", "Nie ma tu nikogo oprocz ciebie.");
            }
        }
        return opis;
    }
    
    /**
     *
     * @param numer
     */
    public void odkryjPokoj(int numer)
    {
        int tab[] = wspolzedne(numer);
        plansza[tab[0]][tab[1]].odkryjPomieszczenie();
    }

    /**
     *
     * @param tab
     */
    public void odkryjPokoj(int tab[])
    {
        plansza[tab[0]][tab[1]].odkryjPomieszczenie();
    }

    /**
     *
     * @param nazwa
     * @param polozenie
     * @return
     */
    public int sprawdzNPC(String nazwa, int polozenie)
    {
        for(int i = 0;i < npc.length;i++)
        {
            if(npc[i].nazwa.equals(nazwa) && npc[i].podajPolozenie() == polozenie)
            {
                return i;
            }
        }
        return -1;
    }

    /**
     *
     * @param nr
     * @return
     */
    public Osoba dajNPC(int nr)
    {
        return npc[nr];
    }
    
    /**
     *
     * @param nr
     */
    public void usunNPC(int nr)
    {
        for(int i = 0;i < npc.length;i++)
        {
            if(npc[i].numer == nr)
            {
                npc[i] = new Osoba();
            }
        }
    }
    
    /**
     *
     * @param nazwa
     * @param polozenie
     * @return
     */
    public Mebel przeszukajPomieszczenieMapa(String nazwa,int polozenie)
    {
        int wsp[] = wspolzedne(polozenie);
        return plansza[wsp[0]][wsp[1]].przeszukaPomieszczenie(nazwa);
    }
    
    /**
     *
     * @param nazwa
     * @param polozenie
     * @return
     */
    public Przedmiot znajdzWKontenerachMapa(String nazwa, int polozenie)
    {
        int wsp[] = wspolzedne(polozenie);
        return plansza[wsp[0]][wsp[1]].znajdzWKontenerach(nazwa);
    }
    
    /**
     *
     * @param komenda
     * @param polozenie
     * @param g
     * @return
     */
    public String uzyjObiektMapa(String komenda,int polozenie,OsobaGracz g)
    {
        int wsp[] = wspolzedne(polozenie);
        return plansza[wsp[0]][wsp[1]].uzyjObiekt(komenda, g);
    }
    
    /**
     *
     * @param gdzie
     * @return
     */
    public int czyJestPrzeciwnik(int gdzie)
    {
        for(int i =0; i < npc.length;i++)
        {
            Osoba o = npc[i];
            if(o.podajPolozenie() == gdzie && o instanceof OsobaPotwor)
            {
                return i;
            }
        }
        return -1;
    }
    
    /**
     *
     * @param nazwa
     * @param polozenie
     */
    public void usunPrzedmiotMapa(String nazwa, int polozenie)
    {
        int wsp[] = wspolzedne(polozenie);
        plansza[wsp[0]][wsp[1]].usunPrzedmiotPomieszczenie(nazwa);
    }
    
    /**
     *
     * @param nazwa
     * @param polozenie
     */
    public void kontenerOtwartyMapa(String nazwa,int polozenie)
    {
        int wsp[] = wspolzedne(polozenie);
        plansza[wsp[0]][wsp[1]].kontenerOtwartyPomieszczenie(nazwa);
    }
    
    /**
     *
     * @return
     */
    public boolean warunekKonca()
    {
        for (Osoba npc1 : npc) {
            if (npc1 instanceof OsobaPotwor) {
                return false;
            }
        }
        return true;
    }
}
