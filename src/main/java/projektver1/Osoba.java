/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projektver1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

/**
 *
 * @author Iwo Ryszkowski
 */
public class Osoba {
    
    /**
     *
     */
    protected int polozenie;
    int numer;
    String nazwa;
    String opis;
    int lvl;
    int sila[];
    int zrecznosc[];
    int szybkosc[];
    int inteligencja[];
    int silaWoli[];
    int wytrzymalosc[];
    int punktyZycia[];
    Efekt[] stany;
    int szczescie;
    boolean stun;
    
    /**
     *
     * @param plik
     * @param numer
     * @param polozenie
     * @throws FileNotFoundException
     */
    public Osoba(String plik,int numer,int polozenie) throws FileNotFoundException
    {
        Scanner odczyt;
        stun = false;
        if(plik.equals("GRACZ"))
        {
            odczyt = new Scanner(new BufferedReader(new FileReader("osoby\\gracz\\GRACZ.txt")));
        }
        else
        {
            odczyt = new Scanner(new BufferedReader(new FileReader("osoby\\" + plik + ".txt")));
        }
        this.numer = numer;
        while(!odczyt.nextLine().equals("NAZWA:"));
        nazwa = odczyt.nextLine();
        while(!odczyt.nextLine().equals("OPIS:"));
        opis = odczyt.nextLine();
        while(!odczyt.nextLine().equals("ATRYBUTY:"));
        sila = new int[2];
        zrecznosc = new int[2];
        szybkosc = new int[2];
        inteligencja = new int[2];
        silaWoli = new int[2];
        punktyZycia = new int[2];
        wytrzymalosc = new int[2];
        sila[0] = sila[1] = Integer.valueOf(odczyt.nextLine());
        zrecznosc[0] = zrecznosc[1] = Integer.valueOf(odczyt.nextLine());
        szybkosc[0] = szybkosc[1] = Integer.valueOf(odczyt.nextLine());
        inteligencja[0] = inteligencja[1] = Integer.valueOf(odczyt.nextLine());
        silaWoli[0] = silaWoli[1] = Integer.valueOf(odczyt.nextLine());
        wytrzymalosc[0] = wytrzymalosc[1] = Integer.valueOf(odczyt.nextLine());
        punktyZycia[0] = punktyZycia[1] = wytrzymalosc[0]*2+5+lvl;
        stany = new Efekt[6];
        for(int i = 0;i < 6;i++)
        {
            stany[i] = new Efekt();
        }
        szczescie = 0;
        while(!odczyt.nextLine().equals("LVL:"));
        lvl = Integer.valueOf(odczyt.nextLine());
        odczyt.close();
        
        this.polozenie = polozenie;
    }
    
    /**
     *
     */
    public Osoba()
    {
        polozenie = 0;
        numer = -1;
        nazwa = "";
        opis = "";
        lvl = 0;
        sila = new int[2];
        zrecznosc = new int[2];
        szybkosc = new int[2];
        inteligencja = new int[2];
        silaWoli = new int[2];
        punktyZycia = new int[2];
        wytrzymalosc = new int[2];
        sila[0] = sila[1] = 0;
        zrecznosc[0] = zrecznosc[1] = 0;
        szybkosc[0] = szybkosc[1] = 0;
        inteligencja[0] = inteligencja[1] = 0;
        silaWoli[0] = silaWoli[1] = 0;
        wytrzymalosc[0] = wytrzymalosc[1] = 0;
        punktyZycia[0] = punktyZycia[1] = 0;
        stany = new Efekt[6];
        for(int i = 0;i < 6;i++)
        {
            stany[i] = new Efekt();
        }
        szczescie = 0;
    }
    
    /**
     *
     * @param przeciwnik
     * @return
     */
    public boolean walkaTrafienie(Osoba przeciwnik)
    {
        if(Testy.test1(zrecznosc[1], przeciwnik.szybkosc[1], szczescie) > 0)
        {
            return true;
        }
        return false;
    }
    
    /**
     *
     * @return
     */
    public int walkaZadajObrazenia()
    {
        return Testy.test1(sila[1],0,szczescie);
    }
    
    /**
     *
     * @param obrazenia
     * @return
     */
    public int walkaPrzyjmijObrazenia(int obrazenia)
    {
        punktyZycia[1] -= obrazenia;
        if(punktyZycia[1] <= 0)
        {
            numer = -1;
            return lvl;
        }
        return 0;
    }
    
    
    /**
     *
     * @param nowePolozenie
     */
    public void ruch(int nowePolozenie)
    {
        polozenie = nowePolozenie;
    }
    
    /**
     *
     * @return
     */
    public int podajPolozenie()
    {
        return polozenie;
    }
    
    /**
     *
     * @return
     */
    public int podajAktualneZdrowie()
    {
        return punktyZycia[1];
    }
    
    /**
     *
     * @return
     */
    public int nastEfekt()
    {
        for(int  i = 0;i < 6;i++)
        {
            if(stany[i].jakiEfekt() == -1)
            {
                return i;
            }
        }
        return -1;
    }
    
    /**
     *
     * @param e
     * @return
     */
    public boolean nalozEfekt(Efekt e)
    {
        int n = nastEfekt();
        if(n == -1)
        {
            return false;
        }
        if(e.jakiEfekt() > 0 && e.jakiEfekt() < 8)
        {
            stany[n] = e;
        }
        switch(e.jakiEfekt())
        {
            case 0:
                ulecz(e.jakZmiana());
                break;
            case 1:
                sila[1] += e.jakZmiana();
            case 2:
                zrecznosc[1] += e.jakZmiana();
                break;
            case 3:
                szybkosc[1] += e.jakZmiana();
                break;
            case 4:
                inteligencja[1] += e.jakZmiana();
                break;
            case 5:
                silaWoli[1] += e.jakZmiana();
                break;
            case 6:
                wytrzymalosc[1] += e.jakZmiana();
                break;
            case 7:
                stun = true;
                break;
            case 8:
                szczescie = 1;
                break;
            case 9:
                szczescie = -1;
                break;
            default:
                break;
        }
        return true;
    }
    
    /**
     *
     * @param ile
     */
    public void ulecz(int ile)
    {
        punktyZycia[1] += ile;
        if(punktyZycia[1] > punktyZycia[0])
        {
            punktyZycia[1] = punktyZycia[0];
        }
    }
    
    /**
     *
     */
    public void nowaTura()
    {
        for(int i = 0;i < 6;i++)
        {
            if(stany[i].jakZmiana() != -1)
            {
                if(stany[i].kolejnaTura() == false)
                {
                    usunStan(i);
                }
            }
            else
            {
                break;
            }
        }
    }
    
    /**
     *
     * @param ktory
     */
    public void usunStan(int ktory)
    {
        switch(stany[ktory].jakiEfekt())
        {
            case 1:
                sila[1] -= stany[ktory].jakZmiana();
                break;
            case 2:
                zrecznosc[1] -= stany[ktory].jakZmiana();
                break;
            case 3:
                szybkosc[1] -= stany[ktory].jakZmiana();
                break;
            case 4:
                inteligencja[1] -= stany[ktory].jakZmiana();
                break;
            case 5:
                silaWoli[1] -= stany[ktory].jakZmiana();
                break;
            case 6:
                wytrzymalosc[1] -= stany[ktory].jakZmiana();
                break;
            case 7:
                stun = false;
                break;
            default:
                break;
        }
        stany[ktory] = new Efekt();
        int nowePozycje[] = {-1,-1,-1,-1,-1,-1};
        int j = 0;
        for(int i = 0;i < 6;i++)
        {
            if(stany[i].jakiEfekt() != -1)
            {
                nowePozycje[j] = i;
                j++;
            }
        }
        for(int i = 0;i < 6;i++)
        {
            if(nowePozycje[i] == -1)
            {
                stany[i] = new Efekt();
            }
            else
            {
                stany[i] = stany[nowePozycje[i]];
            }
        }
    }

}
