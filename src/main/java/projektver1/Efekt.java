/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projektver1;

/**
 *
 * @author Iwo Ryszkowski
 */
public class Efekt {
    private int jaki;
    private int oIle;
    private int ileTur;

    /**
     *
     */
    public Efekt()
    {
        jaki = oIle = ileTur = -1;
    }

    /**
     *
     * @param j
     * @param i
     * @param t
     */
    public Efekt(int j,int i, int t)
    {
        jaki = j;
        oIle = i;
        ileTur = t;
    }
    
    /**
     *
     * @return
     */
    public boolean kolejnaTura()
    {
        ileTur--;
        if(ileTur == 0)
        {
            return false;
        }
        return true;
    }
    
    /**
     *
     * @return
     */
    public int jakiEfekt()
    {
        return jaki;
    }
    
    /**
     *
     * @return
     */
    public int jakZmiana()
    {
        return oIle;
    }
}
