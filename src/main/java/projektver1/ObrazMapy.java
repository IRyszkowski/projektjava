/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projektver1;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Rectangle;
import javax.swing.JPanel;

/**
 *
 * @author Iwo Ryszkowski
 */
public class ObrazMapy extends JPanel{
    ObrazPokoju[][] miniMapa;
    ObrazGracz gracz;
    
    /**
     *
     * @param y
     * @param x
     */
    public ObrazMapy(int y,int x)
    {
        miniMapa = new ObrazPokoju[y][x];
        setLayout(null);
        
        gracz = new ObrazGracz();
        add(gracz);
        gracz.setBounds(new Rectangle(0,0,40,40));
        for(int i = 0;i < y;i++)
        {
            for(int j = 0;j < x;j++)
            {
                miniMapa[i][j] = new ObrazPokoju();
                add(miniMapa[i][j]);
                miniMapa[i][j].setBounds(new Rectangle(0+40*j,0+40*i,40,40));
            }
        }
        setPreferredSize(new Dimension(x*40,y*40));
    }
    
    /**
     *
     * @param tab
     * @param pokoj
     */
    public void odkryjPokoj(int tab[], Pomieszczenie pokoj)
    {
        gracz.setBounds(new Rectangle(tab[1]*40,tab[0]*40,tab[1]*40 + 40,tab[0]*40 + 40));
        gracz.repaint();
        miniMapa[tab[0]][tab[1]].odslon(pokoj);
        miniMapa[tab[0]][tab[1]].repaint();
    }
}
