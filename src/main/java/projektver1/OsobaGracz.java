/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projektver1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.Scanner;

/**
 *
 * @author Iwo Ryszkowski
 */
public class OsobaGracz extends Osoba{
    Przedmiot plecak[];
    Wyposazenie buty;
    Wyposazenie spodnie;
    Wyposazenie rekawice;
    Wyposazenie napiersnik;
    Wyposazenie helm;
    Bron prawa;
    Bron lewa;
    int atak;
    int punktyAkcji[];
    int cap[];
    int exp[];
    
    /**
     *
     * @throws FileNotFoundException
     */
    public OsobaGracz() throws FileNotFoundException {
        super("GRACZ", 1,1);
        punktyAkcji = new int[2];
        punktyAkcji[0] = punktyAkcji[1] = inteligencja[1]*2 + lvl + 10;
        cap = new int[2];
        cap[0] = cap[1] = (int) (sila[1] + wytrzymalosc[1]*0.5 + 10); 
        exp = new int[2];
        exp[0] = lvl*10;
        Scanner odczyt = new Scanner(new BufferedReader(new FileReader("osoby\\gracz\\GRACZ.txt")));
        while(!odczyt.nextLine().equals("EXP:"));
        exp[1] = Integer.valueOf(odczyt.nextLine());
        String rzecz;
        while(!odczyt.nextLine().equals("PLECAK:"));
        int wielkoscPlecaka = Integer.valueOf(odczyt.nextLine());
        plecak = new Przedmiot[wielkoscPlecaka];
        for(int i = 0;i < wielkoscPlecaka;i++)
        {
            rzecz = odczyt.nextLine();
            if(rzecz.equals("null"))
            {
                for(int j = i;j < wielkoscPlecaka;j++)
                {
                    plecak[j] = new Przedmiot();
                }
                i = wielkoscPlecaka;
            }
            else
            {
                if(rzecz.startsWith("2"))
                {
                     plecak[i] = new Wyposazenie(rzecz);
                }
                else if(rzecz.startsWith("3"))
                {
                    plecak[i] = new Bron(rzecz);
                }
                else
                {
                    plecak[i] = new Przedmiot(rzecz);
                }
            }
        }
        atak = -1;
        while(!odczyt.nextLine().equals("WYPOSAZENIE:"));
        rzecz = odczyt.nextLine();
        if(rzecz.equals("null"))
        {
            buty = new Wyposazenie();
        }
        else
        {
            buty = new Wyposazenie(rzecz);
        }
        rzecz = odczyt.nextLine();
        if(rzecz.equals("null"))
        {
            spodnie = new Wyposazenie();
        }
        else
        {
            spodnie = new Wyposazenie(rzecz);
        }
        rzecz = odczyt.nextLine();
        if(rzecz.equals("null"))
        {
            rekawice = new Wyposazenie();
        }
        else
        {
            rekawice = new Wyposazenie(rzecz);
        }
        rzecz = odczyt.nextLine();
        if(rzecz.equals("null"))
        {
            napiersnik = new Wyposazenie();
        }
        else
        {
            napiersnik = new Wyposazenie(rzecz);
        }
        rzecz = odczyt.nextLine();
        if(rzecz.equals("null"))
        {
            helm = new Wyposazenie();
        }
        else
        {
            helm = new Wyposazenie(rzecz);
        }
        rzecz = odczyt.nextLine();
        if(rzecz.equals("null"))
        {
            prawa = new Bron();
        }
        else
        {
            prawa = new Bron(rzecz);
        }
        rzecz = odczyt.nextLine();
        if(rzecz.equals("null"))
        {
            lewa = new Bron();
        }
        else
        {
            lewa = new Bron(rzecz);
        }
        odczyt.close();
    }
    
    /**
     *
     * @param p
     * @return
     */
    public boolean podniesPrzedmiot(Przedmiot p)
    {
        if(plecak[9].numer != -1)
        {
            return false;
        }
        if((cap[1] - p.waga) < 0)
        {
            return false;
        }
        int i;
        for(i = 0;i < 9;i++)
        {
            if(plecak[i].numer == -1)
            {
                break;
            }
        }
        plecak[i] = p;
        cap[1] -= p.waga;
        return true;
    }
    
    /**
     *
     * @param przeciwnik
     * @return
     */
    @Override
    public boolean walkaTrafienie(Osoba przeciwnik)
    {
        
        if(atak == -1)
        {
            return false;
        }
        if(prawa.magiczna == true)
        {
            return true;
        }
        int szansaTrafienia = zrecznosc[1] + prawa.ataki[atak].trafienieModyfikator;
        boolean wynik = Testy.test1(szansaTrafienia, przeciwnik.szybkosc[1], szczescie) > 0;
        return wynik;
    }
    
    /**
     *
     * @return
     */
    @Override
    public int walkaZadajObrazenia()
    {
        if(prawa.magiczna == true)
        {
            return Testy.test1(silaWoli[1] + prawa.obrazenia*prawa.ataki[atak].obrazeniaModyfikator,0,szczescie);
        }
        return Testy.test1(sila[1] + prawa.obrazenia + prawa.ataki[atak].obrazeniaModyfikator,0,szczescie);
    }
    
    /**
     *
     * @param obrazenia
     * @return
     */
    @Override
    public int walkaPrzyjmijObrazenia(int obrazenia)
    {
        obrazenia -= helm.pancerz;
        obrazenia -= rekawice.pancerz;
        obrazenia -= napiersnik.pancerz;
        obrazenia -= buty.pancerz;
        obrazenia -= spodnie.pancerz;
        if(obrazenia > 0)
        {
            punktyZycia[1] -= obrazenia;
        }
        if(punktyZycia[1] <= 0)
        {
            return -1;
        }
        return 0;
    }
    
    /**
     *
     * @param komeda
     * @return
     */
    public int ustawAtak(String komeda)
    {
        if(komeda.equals("czekaj"))
        {
            atak = -1;
            return 2;
        }
        for(int i = 0;i < prawa.ataki.length;i++)
        {
            if(prawa.ataki[i].nazwa.equals(komeda))
            {
                if(prawa.ataki[i].koszt > punktyAkcji[1])
                {
                    return 0;
                }
                punktyAkcji[1] -= prawa.ataki[i].koszt;
                atak = i;
                return 1;
            }
        }
        return -1;
    }
    

    
    /**
     *
     * @param EXP
     * @return
     */
    public int dodajEXP(int EXP)
    {
        exp[1] += EXP;
        int i = lvlUpOblicz();
        lvl += i;
        return i;
    }
    private int lvlUpOblicz()
    {
        int newLVL = 0;
        while(exp[1] >= exp[0])
        {
            exp[1] -= exp[0];
            newLVL++;
            exp[0] = (1 + lvl + newLVL)*(10 + lvl + newLVL);
        }
        return newLVL;
    }
    
    /**
     *
     * @param nazwaP
     * @return
     */
    public Przedmiot przeszukajEkwipunek(String nazwaP)
    {
        for(int i = 0;i < plecak.length;i++)
        {
            if(plecak[i].nazwa.equals(nazwaP))
            {
                return plecak[i];
            }
        }
        return new Przedmiot();
    }
    
    /**
     *
     * @param nazwa
     */
    public void wyrzucPrzedmiot(String nazwa)
    {
        for(int i = 0;i < plecak.length;i++)
        {
            if(plecak[i].dajNazwe().equals(nazwa))
            {
                plecak[i] = new Przedmiot();
                break;
            }
        }
        sortujPlecak();
    }
    
    private void sortujPlecak()
    {
        for(int i = 0;i < plecak.length;i++)
        {
            if(plecak[i].numer == -1)
            {
                for(int j = i;j < plecak.length - 1;j++)
                {
                    plecak[i] = plecak[i+1];
                }
                plecak[plecak.length - 1] = new Przedmiot();
            }
        }
    }
    
    /**
     *
     * @param nazwa
     * @return
     */
    public String uzyjPrzedmiot(String nazwa)
    {
        Przedmiot p = new Przedmiot();
        String s = new String();
        for(int i = 0;i < plecak.length;i++)
        {
            if(plecak[i].dajNazwe().equals(nazwa))
            {
                if(plecak[i].efekty[0].jakiEfekt() == -1)
                {
                    return "Nie mozesz uzyc " + nazwa + "!\n";
                }
                s = plecak[i].uzyj(this);
                if(!s.equals("null"))
                {
                    plecak[i] = new Przedmiot();
                    return s;
                }
                break;
            }
        }
        return "null";
    }
    
    /**
     *
     * @param nazwa
     * @return
     */
    public String zalozWyposazenie(String nazwa)
    {
        Wyposazenie w = new Wyposazenie();
        for(int i = 0;i < plecak.length;i++)
        {
            if(plecak[i].dajNazwe().equals(nazwa))
            {
                if(plecak[i] instanceof Wyposazenie)
                {
                    w = (Wyposazenie) plecak[i];
                    if(sprawdzWymagania(w) == false)
                    {
                        return "Nie spelniasz wymagan by zalozyc " + plecak[i].dajNazwe() + "!\n";
                    }
                    switch(w.gdzie)
                    {
                        case 1:
                            if(buty.numer != -1)
                            {
                                plecak[i] = buty;
                            }
                            else
                            {
                                plecak[i] = new Przedmiot();
                            }
                            buty = w;
                            break;
                        case 2:
                            if(spodnie.numer != -1)
                            {
                                plecak[i] = spodnie;
                            }
                            else
                            {
                                plecak[i] = new Przedmiot();
                            }
                            spodnie = w;
                            break;
                        case 3:
                            if(napiersnik.numer != -1)
                            {
                                plecak[i] = napiersnik;
                            }
                            else
                            {
                                plecak[i] = new Przedmiot();
                            }
                            napiersnik = w;
                            break;
                        case 4:
                            if(rekawice.numer != -1)
                            {
                                plecak[i] = rekawice;
                            }
                            else
                            {
                                plecak[i] = new Przedmiot();
                            }
                            rekawice = w;
                            break;
                        case 5:
                            if(helm.numer != -1)
                            {
                                plecak[i] = helm;
                            }
                            else
                            {
                                plecak[i] = new Przedmiot();
                            }
                            helm = w;
                            break;
                        case 6:
                            if(lewa.numer == -2)
                            {
                                lewa.numer = -1;
                            }
                            if(prawa.numer != -1)
                            {
                                plecak[i] = prawa;
                            }
                            else
                            {
                                plecak[i] = new Przedmiot();
                            }
                            prawa = (Bron)w;
                            break;
                        case 7:
                            if(lewa.numer == -2)
                            {
                                prawa.numer = -1;
                                plecak[i] = prawa;
                            }
                            else if(lewa.numer > -1)
                            {
                                plecak[i] = lewa;
                            }
                            else
                            {
                                plecak[i] = new Przedmiot();
                            }
                            
                            lewa = (Bron)w;
                            break;
                        case 8:
                            if(prawa.numer != -1)
                            {
                                plecak[i] = prawa;
                                if(lewa.numer != -1)
                                {
                                    podniesPrzedmiot(lewa);
                                }
                            }
                            else if(lewa.numer != -1)
                            {
                                plecak[i] = lewa;
                            }
                            else
                            {
                                plecak[i] = new Przedmiot();
                            }
                            lewa.numer = -2;
                            prawa = (Bron)w;
                            break;
                        
                    }
                    sortujPlecak();
                    return "Zalozyles " + nazwa;
                }
                else
                {
                    return "Nie da sie wyposazyc: " + nazwa;
                }
            }
        }
        return "Nie masz " + nazwa + " w ekwipunku!\n";
    }
    
    /**
     *
     * @param w
     * @return
     */
    public boolean sprawdzWymagania(Wyposazenie w)
    {
        if(sila[1] < w.wymSila)
        {
            return false;
        }
        if(zrecznosc[1] < w.wymZrecznosc)
        {
            return false;
        }
        if(szybkosc[1] < w.wymSzybkosc)
        {
            return false;
        }
        if(inteligencja[1] < w.wymInteligencja)
        {
            return false;
        }
        if(wytrzymalosc[1] < w.wymWytrzymalosc)
        {
            return false;
        }
        if(silaWoli[1] < w.wymSilaWoli)
        {
            return false;
        }
        return true;
    }
    
    String opiszAtaki()
    {
        String opisAtakow = "HP: " + punktyZycia[1] + "   PA: " + punktyAkcji[1] + "\n";
        opisAtakow += "0.czekaj +1PA\n";
        opisAtakow += "Prawa:\n";
        opisAtakow += prawa.opiszAtaki();
        if(lewa.numer > 0)
        {
            opisAtakow += "Lewa:\n";
            opisAtakow += lewa.opiszAtaki();
        }
        return opisAtakow;
    }
    
    /**
     *
     */
    @Override
    public void nowaTura()
    {
        super.nowaTura();
        dodajPA();
    }
    
    /**
     *
     * @param ile
     */
    public void dodajPA(int ile)
    {
        punktyAkcji[1] += ile;
        if(punktyAkcji[1] > punktyAkcji[0])
        {
            punktyAkcji[1] = punktyAkcji[0];
        }
    }
    
    /**
     *
     */
    public void dodajPA()
    {
        punktyAkcji[1]++;
        if(punktyAkcji[1] > punktyAkcji[0])
        {
            punktyAkcji[1] = punktyAkcji[0];
        }
    }
    
    /**
     *
     * @return
     */
    public String opiszSie()
    {
        String ret = opis + " Masz na sobie ";
        if(helm.numer != -1)
        {
            ret += helm.nazwa + ", ";
        }
        if(rekawice.numer != -1)
        {
            ret += rekawice.nazwa + ", ";
        }
        if(napiersnik.numer != -1)
        {
            ret += napiersnik.nazwa + ", ";
        }
        if(helm.numer != -1)
        {
            ret += helm.nazwa + ", ";
        }
        if(ret.endsWith("sobie "))
        {
            ret = opis + "Nie masz na sobie żadnego pancerza. ";
        }
        ret += "Do walki uzywasz ";
        if(prawa.numer != -1)
        {
            ret += prawa.nazwa;
            if(lewa.numer > 0)
            {
                ret += " i " + lewa.nazwa;
            }
        }
        else
        {
            ret += "wlasnych piesci. ";
        }
        ret += "\nW plecaku masz:";
        for(int i = 0;i < plecak.length;i++)
        {
            if(plecak[i].numer == -1)
            {
                break;
            }
            ret += "\n" + plecak[i].nazwa + "\n";
        }
        if(ret.endsWith("masz:"))
        {
            ret = ret.replaceAll("masz:", "nie masz nic");
        }
        ret += ".";
        return ret;
    }
    
    /**
     *
     * @return
     */
    public String wPlecaku()
    {
        String opisP = "W plecaku masz:";
        for(int i = 0;i < plecak.length;i++)
        {
            if(plecak[i].numer != -1)
            {
                opisP += "\n" + plecak[i].dajNazwe();
            }
        }
        if(opisP.equals("W plecaku masz:"))
        {
            opisP = "Twoj plecak jest pusty.";
        }
        return opisP;
    }
    
    /**
     *
     * @return
     */
    public String bron()
    {
        String opisB = "Prawa: ";
        if(prawa.numer != -1)
        {
            opisB += prawa.dajNazwe();
        }
        else
        {
            opisB += "brak";
        }
        opisB += "\nLewa: ";
        if(lewa.numer > -1)
        {
            opisB += lewa.dajNazwe();
        }
        else
        {
            opisB += "brak";
        }
        return opisB;
    }
    
    /**
     *
     * @return
     */
    public String pancerz()
    {
        String opisP = "Helm: ";
        if(helm.numer != -1)
        {
            opisP += helm.dajNazwe();
        }
        else
        {
            opisP += "brak";
        }
        opisP += "\nNapiersnik: ";
        if(napiersnik.numer != -1)
        {
            opisP += napiersnik.dajNazwe();
        }
        else
        {
            opisP += "brak";
        }
        opisP += "\nRekawice: ";
        if(rekawice.numer != -1)
        {
            opisP += rekawice.dajNazwe();
        }
        else
        {
            opisP += "brak";
        }
        opisP += "\nSpodnie: ";
        if(spodnie.numer != -1)
        {
            opisP += spodnie.dajNazwe();
        }
        else
        {
            opisP += "brak";
        }
        opisP += "\nButy: ";
        if(buty.numer != -1)
        {
            opisP += buty.dajNazwe();
        }
        else
        {
            opisP += "brak";
        }
        return opisP;
    }
    
    /**
     *
     * @return
     */
    public String dajExp()
    {
        String expS = "Posiadane punkty doswiadczenia: " + exp[1] + "\n";
        expS += "Punkty doswiadczenia poterzebne na lolejny poziom : " + exp[0] + "\n";
        return expS;
    }
    
    /**
     *
     * @return
     */
    public String dajLvl()
    {
        String lvlS = "Masz " + Integer.toString(lvl) + " poziom\n";
        return lvlS;
    }
    
    /**
     *
     * @return
     */
    public String dajStat()
    {
        String stat = "Atrybut: WartoscBazowa/WartoscAktualna\n";
        stat += "sila: " + sila[0] + "/" + sila[1] + "\n";
        stat += "inteligencja: " + inteligencja[0] + "/" + inteligencja[1] + "\n";
        stat += "sila woli: " + silaWoli[0] + "/" + silaWoli[1] + "\n";
        stat += "wytrzymalosc: " + wytrzymalosc[0] + "/" + wytrzymalosc[1] + "\n";
        stat += "punkty zycia: " + punktyZycia[0] + "/" + punktyZycia[1] + "\n";
        return stat;
    }
    
    /**
     *
     */
    public void odnowPA()
    {
        punktyAkcji[1] = punktyAkcji[0];
    }
    
    /**
     *
     * @param jaka
     * @param ile
     */
    public void zwiekrzStat(int jaka,int ile)
    {
        switch(jaka)
        {
            case 1:
                sila[0] += ile;
                sila[1] += ile;
                break;
            case 2:
                zrecznosc[0] += ile;
                zrecznosc[1] += ile;
                break;
            case 3:
                szybkosc[0] += ile;
                szybkosc[1] += ile;
                break;
            case 4:
                inteligencja[0] += ile;
                inteligencja[1] += ile;
                punktyAkcji[1] += ile*2;
                punktyAkcji[0] += ile*2;
                break;
            case 5:
                silaWoli[0] += ile;
                silaWoli[1] += ile;
                break;
            case 6:
                wytrzymalosc[0] += ile;
                wytrzymalosc[1] += ile;
                punktyZycia[0] += ile*2;
                punktyZycia[0] = punktyZycia[1];
                break;
            default:
                break;
        }
    }
    
    /**
     *
     * @throws IOException
     */
    public void zapisz() throws IOException
    {
        File g = new File("osoby\\gracz\\GRACZ.txt");
        g.createNewFile();
        BufferedWriter zapis = Files.newBufferedWriter(g.toPath(), Charset.forName("US-ASCII"));
        zapis.write("NAZWA:");
        zapis.newLine();
        zapis.write("Ja");
        zapis.newLine();
        zapis.write("OPIS:");
        zapis.newLine();
        zapis.write("To ty.");
        zapis.newLine();
        zapis.write("ATRYBUTY:");
        zapis.newLine();
        zapis.write(Integer.toString(sila[0]));
        zapis.newLine();
        zapis.write(Integer.toString(zrecznosc[0]));
        zapis.newLine();
        zapis.write(Integer.toString(szybkosc[0]));
        zapis.newLine();
        zapis.write(Integer.toString(inteligencja[0]));
        zapis.newLine();
        zapis.write(Integer.toString(silaWoli[0]));
        zapis.newLine();
        zapis.write(Integer.toString(wytrzymalosc[0]));
        zapis.newLine();
        zapis.write("LVL:");
        zapis.newLine();
        zapis.write(Integer.toString(lvl));
        zapis.newLine();
        zapis.write("EXP:");
        zapis.newLine();
        zapis.write(Integer.toString(exp[1]));
        zapis.newLine();
        zapis.write("PLECAK:");
        zapis.newLine();
        zapis.write(Integer.toString(plecak.length));
        zapis.newLine();
        for(int i = 0;i < plecak.length;i++)
        {
            if(plecak[i].numer != -1)
            {
                if(plecak[i] instanceof Bron)
                {
                    zapis.write("3" + plecak[i].nazwa);
                    zapis.newLine();
                }
                else if(plecak[i] instanceof Wyposazenie)
                {
                    zapis.write("2" + plecak[i].nazwa);
                    zapis.newLine();
                }
                else
                {
                    zapis.write(plecak[i].nazwa);
                    zapis.newLine();
                }
            }
            else
            {
                zapis.write("null");
                zapis.newLine();
            }
        }
        zapis.write("WYPOSAZENIE:");
        zapis.newLine();
        if(buty.numer == -1)
        {
            zapis.write("null");
            zapis.newLine();
        }
        else
        {
            zapis.write(buty.nazwa);
            zapis.newLine();
        }
        if(spodnie.numer == -1)
        {
            zapis.write("null");
            zapis.newLine();
        }
        else
        {
            zapis.write(spodnie.nazwa);
            zapis.newLine();
        }
        if(rekawice.numer == -1)
        {
            zapis.write("null");
            zapis.newLine();
        }
        else
        {
            zapis.write(rekawice.nazwa);
            zapis.newLine();
        }
        if(napiersnik.numer == -1)
        {
            zapis.write("null");
            zapis.newLine();
        }
        else
        {
            zapis.write(napiersnik.nazwa);
            zapis.newLine();
        }
        if(helm.numer == -1)
        {
            zapis.write("null");
            zapis.newLine();
        }
        else
        {
            zapis.write(helm.nazwa);
            zapis.newLine();
        }
        if(prawa.numer == -1)
        {
            zapis.write("null");
            zapis.newLine();
        }
        else
        {
            zapis.write(prawa.nazwa);
            zapis.newLine();
        }
        if(lewa.numer == -1)
        {
            zapis.write("null");
            zapis.newLine();
        }
        else
        {
            zapis.write(lewa.nazwa);
            zapis.newLine();
        }
        zapis.flush();
        zapis.close();
    }
}
