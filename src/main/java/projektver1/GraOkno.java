/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projektver1;

import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import static java.lang.Math.pow;
import java.util.Scanner;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author Iwo Ryszkowski
 */
public class GraOkno extends JFrame{
    String bierzacaKomeda;
    Vector<String> komendy;
    int it;
    JTextArea narracja;
    JTextField akcja;
    Mapa mapa;
    int stan;
    int lvlUp;
    ObrazMapy miniMapa;
    OsobaGracz gracz;
    OsobaPotwor przeciwnik;
    
    /**
     *
     * @param nazwa
     * @throws FileNotFoundException
     */
    public GraOkno(String nazwa) throws FileNotFoundException
    {
        super(nazwa);
        lvlUp = 0;
        przeciwnik = new OsobaPotwor();
        stan = -1;
        komendy = new Vector();
        it = -1;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        narracja = new JTextArea(35,75);
        narracja.setLineWrap(true);
        narracja.setEditable(false);
        wyswietlInstrukcje();
        JScrollPane scroll = new JScrollPane (narracja, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        akcja = new JTextField();
        akcja.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                switch (e.getKeyCode()) {
                    case KeyEvent.VK_ENTER:
                        bierzacaKomeda = akcja.getText();
                        komendy.add(bierzacaKomeda);
                {
                    try {
                        obslugaKomend();
                    } catch (FileNotFoundException ex) {
                        Logger.getLogger(GraOkno.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(GraOkno.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                        if(stan == 2)
                        {
                            narracja.append("Czy chcesz zaczac od nowa?(TAK/NIE)");
                            
                        }
                        else if(stan == 0)
                        {
                            int ktory = czyJestPrzeciwnik();
                            if(ktory != -1)
                            {
                                przeciwnik = (OsobaPotwor)mapa.dajNPC(ktory);
                                narracja.append("Zatakowal cie " + przeciwnik.nazwa + "\n");
                                opisPrzeciwnika();
                                stan = 1;
                                do
                                {
                                    int obrazenia = przeciwnik.walkaZadajObrazenia();
                                    narracja.append("Przeciwnik zadal ci " + obrazenia + " punktow obrazen\n");
                                    if(gracz.walkaPrzyjmijObrazenia(obrazenia) == -1)
                                    {
                                       narracja.append("Smierc!");
                                       stan = 2;
                                    }
                                    przeciwnik.nowaTura();
                                    gracz.nowaTura();
                                }while(gracz.stun == true);
                                narracja.append(gracz.opiszAtaki());
                            }
                        }
                        akcja.setText("");
                        it = komendy.size()-1;
                        break;
                    case KeyEvent.VK_UP:
                        if(it<0)
                        {
                            it = 0;
                        }   akcja.setText(komendy.get(it));
                        it--;
                        break;
                    case KeyEvent.VK_DOWN:
                        if(it == -1)
                        {
                            it++;
                        }   it++;
                        if(it==komendy.size())
                        {
                            akcja.setText("");
                            it--;
                        }
                        else
                        {
                            akcja.setText(komendy.get(it));
                        }   break;
                    default:
                        break;
                }
            }
        });
        add(scroll,BorderLayout.WEST);
        add(akcja,BorderLayout.SOUTH);
        pack();
        setVisible(true);
        narracja.append("Wpisz nazwe mapy:(default \"mapa\")");
    }
    
    private void obslugaKomend() throws FileNotFoundException, IOException
    {
        bierzacaKomeda = bierzacaKomeda.trim();
        boolean poprawnoscKomedy = true;
        narracja.append("\n>"+bierzacaKomeda+"\n");
        if(bierzacaKomeda.equals("help"))
        {
            try {
                wyswietlInstrukcje();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(DungeonCrawler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else
        {
            switch(stan)
            {
                case -1:

                    poprawnoscKomedy = wyborMapy();
                    break;
                case 0:
                    poprawnoscKomedy = obslugaKomendStan0();
                    break;
                case 1:
                    int obs1 = obslugaKomendStan1();
                    if(obs1 >= 0)
                    {
                        poprawnoscKomedy = true;
                        if(lvlUp > 0)
                        {
                            int punkty = 0;
                            for(int i = 0;i < lvlUp;i++)
                            {
                                punkty += pow(10,((gracz.lvl - i)/25)) *5;
                            }
                            lvlUp = punkty;
                            narracja.append("Masz do rozdania " + lvlUp + " punktow.\n Wybierz Atrybut ktory chcesz zwiekrzyc:\n");
                            narracja.append("sila\n");
                            narracja.append("zrecznosc\n");
                            narracja.append("szybkosc\n");
                            narracja.append("inteligencja\n");
                            narracja.append("sila woli\n");
                            narracja.append("wytrzymalosc\n");
                            stan = 3;
                        }
                        else if(mapa.warunekKonca())
                        {
                            narracja.append("Pokonales wszystkich przeciwnikow. Loch zostal ukonczony.");
                            narracja.append("Czy chcesz zapisac postac?(TAK/NIE)");
                            stan = 4;
                        }
                    }
                    else
                    {
                        poprawnoscKomedy = false;
                    }
                    
                    break;
                case 2:
                    poprawnoscKomedy = smierc();
                    break;
                case 3:
                    poprawnoscKomedy = obslugaKomendStan3();
                    break;
                case 4:
                    poprawnoscKomedy = obslugaKomendStan4();
                    break;
                case 5:
                    this.dispose();
                    break;
                default:
                   break;
            }
        }
        if(poprawnoscKomedy == false)
        {
            narracja.append("Bledna komeda!\n");
        }
        narracja.setCaretPosition(narracja.getDocument().getLength());
    }
    
    private boolean obslugaKomendStan0()
    {
        if(bierzacaKomeda.startsWith("rozejrzyj sie"))
        {
            if(bierzacaKomeda.equals("rozejrzyj sie"))
            {
                narracja.append(mapa.rozejzyjSie(gracz.podajPolozenie(),0)+"\n");
            }
            else if(bierzacaKomeda.equals("rozejrzyj sie na polnoc"))
            {
                narracja.append(mapa.rozejzyjSie(gracz.podajPolozenie(),1)+"\n");
            }
            else if(bierzacaKomeda.equals("rozejrzyj sie na wschod"))
            {
                narracja.append(mapa.rozejzyjSie(gracz.podajPolozenie(),2)+"\n");
            }
            else if(bierzacaKomeda.equals("rozejrzyj sie na poludnie"))
            {
                narracja.append(mapa.rozejzyjSie(gracz.podajPolozenie(),3)+"\n");
            }
            else if(bierzacaKomeda.equals("rozejrzyj sie na zachod"))
            {
                narracja.append(mapa.rozejzyjSie(gracz.podajPolozenie(),4)+"\n");
            }
            else 
            {
                return false;
            }
        }
        else if(bierzacaKomeda.equals("exp") || bierzacaKomeda.equals("EXP") || bierzacaKomeda.equals("Exp"))
        {
            narracja.append(gracz.dajExp());
        }
        else if(bierzacaKomeda.startsWith("lvl") || bierzacaKomeda.startsWith("poziom"))
        {
            narracja.append(gracz.dajLvl());
        }
        else if(bierzacaKomeda.startsWith("uzyj "))
        {
            uzyjOP();
        }
        else if(bierzacaKomeda.equals("status"))
        {
            narracja.append(gracz.dajStat());
        }
        else if(bierzacaKomeda.startsWith("wyposaz "))
        {
            narracja.append(gracz.zalozWyposazenie(bierzacaKomeda.replaceAll("wyposaz ", "")) + "\n");
        }
        else if(bierzacaKomeda.startsWith("zaloz "))
        {
            narracja.append(gracz.zalozWyposazenie(bierzacaKomeda.replaceAll("zaloz ", "")) + "\n");
        }
        else if(bierzacaKomeda.startsWith("plecak"))
        {
            obejrzyjEkwipunek();
        }
        else if(bierzacaKomeda.startsWith("bron"))
        {
            obejrzyjBron();
        }
        else if(bierzacaKomeda.startsWith("pancerz"))
        {
            obejrzyjPancerz();
        }
        else if(bierzacaKomeda.startsWith("obejrzyj "))
        {
            obejrzyj();
        }
        else if(bierzacaKomeda.startsWith("wez "))
        {
            wezPrzedmiot();
        }
        else if(bierzacaKomeda.startsWith("podnies "))
        {
            wezPrzedmiot();
        }
        else if(bierzacaKomeda.startsWith("otworz "))
        {
            otwozKontener();
        }
        else if(bierzacaKomeda.startsWith("idz na "))
        {
            int kierunek;
            bierzacaKomeda = bierzacaKomeda.replaceFirst("idz na ", "");
            switch(bierzacaKomeda)
            {
                case "N":
                case "PN":
                case "polnoc":
                    kierunek = 0;
                    break;
                case "WS":
                case "E":
                case "wschod":
                    kierunek = 1;
                    break;
                case "PL":
                case "S":
                case "poludnie":
                    kierunek = 2;
                    break;
                case "Z":
                case "W":
                case "zachod":
                    kierunek = 3;
                    break;
                default:
                    kierunek = -1;
                    narracja.append("nie ma takiego kierunku!\n");
                    break;
            }
            int nowePolozenie = mapa.ruch(gracz.podajPolozenie(), kierunek);
            if(nowePolozenie > 0)
            {
                gracz.ruch(nowePolozenie);
                int tab[] = mapa.wspolzedne(gracz.podajPolozenie());
                miniMapa.odkryjPokoj(tab, mapa.plansza[tab[0]][tab[1]]);
                switch(bierzacaKomeda)
                {
                    case "polnoc":
                        narracja.append("Przechodzisz przez polnocne drzwi.\n");
                        break;
                    case "wschod":
                        narracja.append("Przechodzisz przez wchodnie drzwi.\n");
                        break;
                    case "poludnie":
                        narracja.append("Przechodzisz przez poludniowe drzwi.\n");
                        break;
                    case "zachod":
                        narracja.append("Przechodzisz przez zachodnie drzwi.\n");
                        break;
                    default:
                        break;
                }
                
            }
            else if(nowePolozenie == 0)
            {
                narracja.append("Nie ma drzwi w tym kierunku!\n");
            }
        }
        else if(bierzacaKomeda.startsWith("atakuj "))
        {
            int ktory = mapa.sprawdzNPC(bierzacaKomeda.replaceAll("atakuj ", ""), gracz.podajPolozenie());
            if(ktory != -1)
            {
                przeciwnik = (OsobaPotwor)mapa.dajNPC(ktory);
                narracja.append("atakujesz " + przeciwnik.nazwa + "\n");
                opisPrzeciwnika();
                stan = 1;
                narracja.append(gracz.opiszAtaki());
            }
            else
            {
                narracja.append("Nie ma takiego przeciwnika w pomieszczeniu!\n");
            }
        }
        else
        {
            return false;
        }
        return true;
    }
    
    private int obslugaKomendStan1()
    {
        int wynik = gracz.ustawAtak(bierzacaKomeda);
        if(wynik > 0)
        {
            int exp = 0;
            if(wynik == 2)
            {
                gracz.dodajPA();
                gracz.nowaTura();
                przeciwnik.nowaTura();
                opisPrzeciwnika();
                narracja.append(gracz.opiszAtaki());
                return 1;
            }
            else
            {
                if(gracz.walkaTrafienie(przeciwnik) || przeciwnik.stun)
                {
                    int obrazenia = gracz.walkaZadajObrazenia();
                    narracja.append("Zadales " + obrazenia + " punktow obrazen\n");
                    exp = przeciwnik.walkaPrzyjmijObrazenia(obrazenia);
                }
                else
                {
                    narracja.append("nie trafiles\n");
                }
            }
            if(exp > 0 )
            {
               narracja.append("Zabiles " + przeciwnik.nazwa + " i zdobyles " + exp + " punktow doswiadczenia!\n");
               lvlUp = gracz.dodajEXP(exp);
               mapa.usunNPC(przeciwnik.numer);
               stan = 0;
               gracz.nowaTura();
               gracz.odnowPA();
               return lvlUp;
            }
            else if(przeciwnik.stun == false)
            {
                do
                {
                    if(przeciwnik.walkaTrafienie(gracz))
                    {
                        int obrazenia = przeciwnik.walkaZadajObrazenia();
                        narracja.append(przeciwnik.nazwa + " zadal ci " + obrazenia + " punktow obrazen\n");
                        if(gracz.walkaPrzyjmijObrazenia(obrazenia) == -1)
                        {
                           narracja.append("Smierc!");
                           stan = 2;
                           return 1;
                        }
                        gracz.nowaTura();
                        przeciwnik.nowaTura();
                    }
                    else
                    {
                        narracja.append(przeciwnik.nazwa + "nie trafil!\n");
                    }
                }while(gracz.stun == true);
            }
            else
            {
                przeciwnik.nowaTura();
                gracz.nowaTura();
            }
            opisPrzeciwnika();
            narracja.append(gracz.opiszAtaki());
            return 1;
        }
        else if(wynik == 0)
        {
            narracja.append("Nie masz wystarczajaco duza PA aby wykonac ten atak!\n");
            opisPrzeciwnika();
            narracja.append(gracz.opiszAtaki());
            return 0;
        }
        return -1;
    }
    
    private boolean obslugaKomendStan3()
    {
        int co = 0;
        switch(bierzacaKomeda)
        {
            case "sila":
                co = 1;
                break;
            case "zrecznosc":
                co = 2;
                break;
            case "szybkosc":
                co = 3;
                break;
            case "inteligencja":
                co = 4;
                break;
            case "sila woli":
                co = 5;
                break;
            case "wytrzymalosc":
                co = 6;
                break;
            default:
                return false;
        }
        gracz.zwiekrzStat(co,1);
        lvlUp--;
        if(lvlUp == 0)
        {
            stan = 0;
            if(mapa.warunekKonca())
            {
                narracja.append("Pokonales wszystkich przeciwnikow. Loch zostal ukonczony.");
                narracja.append("Czy chcesz zapisac postac?(TAK/NIE)");
                stan = 4;
            }
        }
        else
        {
            narracja.append("Masz do rozdania " + lvlUp + " punktow.\n Wybierz Atrybut ktory chcesz zwiekrzyc:\n");
            narracja.append("sila\n");
            narracja.append("zrecznosc\n");
            narracja.append("szybkosc\n");
            narracja.append("inteligencja\n");
            narracja.append("sila woli\n");
            narracja.append("wytrzymalosc\n");
        }
        return true;
    }
    private boolean obslugaKomendStan4() throws IOException
    {
        if(bierzacaKomeda.equals("TAK"))
        {
            gracz.zapisz();
        }
        else if(bierzacaKomeda.equals("NIE"))
        {
            
        }
        else
        {
            return false;
        }
        System.out.println(gracz.exp[1]);
        narracja.append("KONIEC\n Nacisnij ENTER aby zamknac");
        System.out.println(gracz.exp[1]);
        stan = 5;
        return true;
    }
    private void wyswietlInstrukcje() throws FileNotFoundException
    {
        Scanner odczyt = new Scanner(new BufferedReader(new FileReader(new File("opisy\\instrukcje.txt"))));
        while(odczyt.hasNextLine())
        {
            narracja.append(odczyt.nextLine()+"\n");
        }
        odczyt.close();
    }
    
    private void obejrzyj()
    {
        String komenda = bierzacaKomeda.replaceAll("obejrzyj ", "");
        if(komenda.equals("sie"))
        {
            narracja.append(gracz.opiszSie());
            return;
        }
        if(obejrzyjWyposazenie(komenda))
        {
            return;
        }
        if(obejrzyjEkwipunek(komenda))
        {
            return;
        }
        if(obejrzyjObiekt(komenda))
        {
            return;
        }
        if(obejrzyjKontenery(komenda))
        {
            return;
        }
        narracja.append("nie znaleziono " + komenda + "!\n");
    }
    
    private boolean obejrzyjWyposazenie(String komenda)
    {
        if(komenda.equals(gracz.helm.nazwa))
        {
            narracja.append(gracz.helm.dajOpis() + "\n");
        }
        else if(komenda.equals(gracz.buty.nazwa))
        {
            narracja.append(gracz.buty.dajOpis() + "\n");
        }
        else if(komenda.equals(gracz.spodnie.nazwa))
        {
            narracja.append(gracz.spodnie.dajOpis() + "\n");
        }
        else if(komenda.equals(gracz.napiersnik.nazwa))
        {
            narracja.append(gracz.napiersnik.dajOpis() + "\n");
        }
        else if(komenda.equals(gracz.prawa.nazwa))
        {
            narracja.append(gracz.prawa.dajOpis() + "\n");
        }
        else if(komenda.equals(gracz.lewa.nazwa))
        {
            narracja.append(gracz.lewa.dajOpis() + "\n");
        }
        else
        {
            return false;
        }
        return true;
    }
    
    private boolean obejrzyjEkwipunek(String komenda)
    {
        Przedmiot p = gracz.przeszukajEkwipunek(komenda);
        if(p.numer == -1)
        {
            return false;
        }
        narracja.append(p.dajOpis() + "\n");
        return true;
    }
    private void obejrzyjEkwipunek()
    {
        narracja.append(gracz.wPlecaku());
    }
    private void obejrzyjBron()
    {
        narracja.append(gracz.bron() + "\n");
    }
    private void obejrzyjPancerz()
    {
        narracja.append(gracz.pancerz() + "\n");
    }
    private boolean obejrzyjObiekt(String komenda)
    {
        Mebel o = mapa.przeszukajPomieszczenieMapa(komenda,gracz.podajPolozenie());
        if(o.numer == -1)
        {
            return false;
        }
        narracja.append(o.podajOpis() + "\n");
        return true;
    }
    
    private boolean obejrzyjKontenery(String komenda)
    {
        Przedmiot p = mapa.znajdzWKontenerachMapa(komenda, gracz.podajPolozenie());
        if(p.numer == -1)
        {
            return false;
        }
        narracja.append(p.dajOpis() + "\n");
        return true;
    }
    
    private void opisPrzeciwnika()
    {
        narracja.append(przeciwnik.nazwa + " HP: " + przeciwnik.podajAktualneZdrowie() + "\n");
    }
    
    private void uzyjOP()
    {
        String komenda = bierzacaKomeda.replaceAll("uzyj ", "");
        String narr;
        narr = uzyjPrzedmiot(komenda);
        if(narr.equals("null"))
        {
            narr = uzyjObiekt(komenda);
            if(narr.equals("null"))
            {
                narracja.append("nie znaleziono " + komenda + "!\n");
                return;
            }
            narracja.append(narr);
            return;
        }
        narracja.append(narr);
    }
    
    private String uzyjObiekt(String komenda)
    {
        return mapa.uzyjObiektMapa(komenda, gracz.podajPolozenie(), gracz);
    }
    
    private String uzyjPrzedmiot(String komenda)
    {
        return gracz.uzyjPrzedmiot(komenda);
    }
    
    private boolean smierc() throws FileNotFoundException
    {
        if(bierzacaKomeda.toUpperCase().equals("TAK"))
        {
            restart();
            return true;
        }
        else if(bierzacaKomeda.toUpperCase().equals("NIE"))
        {
            narracja.append("KONIEC\n Nacisnij ENTER aby zamknac");
            stan = 5;
            return true;
        }
        return false;
    }
    
    private int czyJestPrzeciwnik()
    {
        return mapa.czyJestPrzeciwnik(gracz.podajPolozenie());
    }
    
    private void restart() throws FileNotFoundException
    {
        setVisible(false);
        przeciwnik = new OsobaPotwor();
        stan = -1;
        
        komendy = new Vector();
        it = -1;
        narracja.setText("");
        wyswietlInstrukcje();
        narracja.append("Podaj nazwe mapy.");
        setVisible(true);
    }
    
    private void zamknij()
    {
        this.dispose();
    }
    
    private void wezPrzedmiot()
    {
        String nazwa = bierzacaKomeda.replaceAll("wez ", "");
        nazwa = nazwa.replaceAll("podnies ", "");
        Przedmiot p = mapa.znajdzWKontenerachMapa(nazwa, gracz.podajPolozenie());
        if(p.numer != -1)
        {
            mapa.usunPrzedmiotMapa(nazwa, gracz.podajPolozenie());
            if(gracz.podniesPrzedmiot(p) == false)
            {
                narracja.append("Nie mozesz podniesc " + p.nazwa + "!\n");
            }
            else
            {
                narracja.append("Podniosles " + p.nazwa + "!\n");
            }
            return;
        }
        narracja.append("Nie znaleziono takiego przedmiotu!\n");
    }
    
    private void otwozKontener()
    {
        String nazwa = bierzacaKomeda.replaceAll("otworz ", "");
        Mebel o = new Mebel();
        o = mapa.przeszukajPomieszczenieMapa(nazwa,gracz.podajPolozenie());
        if(o instanceof Kontener)
        {
            Kontener k = (Kontener)o;
            k.probaOtwarcia(gracz);
            narracja.append(k.podajOpis() + "\n");
            System.out.println(gracz.podajPolozenie());
            mapa.kontenerOtwartyMapa(k.nazwa,gracz.podajPolozenie());
            return;
        }
        narracja.append("Nie znaleziono takiego kontenera!\n");
    }
    
    private boolean wyborMapy() throws FileNotFoundException
    {
        File tmp = new File("mapa//" + bierzacaKomeda + ".txt");
        if(!tmp.canRead())
        {
            narracja.append("Nie ma takiej mapy!");
            return true;
        }
        mapa = new Mapa(bierzacaKomeda);
        miniMapa = new ObrazMapy(mapa.plansza.length,mapa.plansza[0].length);
        gracz = new OsobaGracz();
        mapa.odkryjPokoj(gracz.podajPolozenie());
        int tab[] = mapa.wspolzedne(gracz.podajPolozenie());
        miniMapa.odkryjPokoj(tab, mapa.plansza[tab[0]][tab[1]]);
        add(miniMapa,BorderLayout.CENTER);
        pack();
        setVisible(false);
        setVisible(true);
        stan = 0;
        return true;
    }
}
