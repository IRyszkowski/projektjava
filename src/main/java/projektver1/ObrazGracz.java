/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projektver1;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JComponent;

/**
 *
 * @author Iwo Ryszkowski
 */
public class ObrazGracz extends JComponent{
    private BufferedImage gracz;
    
    /**
     *
     */
    public ObrazGracz()
    {
        super();
        try {
		gracz = ImageIO.read(new File("mapapng\\kropka.png"));
	} catch (IOException e) {
		System.err.println("Blad odczytu obrazka");
		e.printStackTrace();
	}
        setPreferredSize(new Dimension(gracz.getWidth(), gracz.getHeight()));
    }
    
    @Override
    public void paintComponent(Graphics g) {
	Graphics2D g2d = (Graphics2D) g;
        g2d.drawImage(gracz, 0, 0, this);
    }
}
