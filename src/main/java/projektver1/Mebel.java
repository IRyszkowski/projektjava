/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projektver1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

/**
 *
 * @author Iwo Ryszkowski
 */
public class Mebel implements Uzywalne{

    /**
     *
     */
    protected int numer;

    /**
     *
     */
    protected String nazwa;

    /**
     *
     */
    protected String opis;
    boolean uzyte;
    Efekt efekt;
    
    /**
     *
     * @param nr
     * @throws FileNotFoundException
     */
    public Mebel(int nr) throws FileNotFoundException
    {
        numer = nr;
        Scanner odczyt;
        if(this instanceof Kontener)
        {
            odczyt = new Scanner(new BufferedReader(new FileReader(new File("obiekty\\kontenery\\" + nr + ".txt"))));
        }
        else
        {
            odczyt = new Scanner(new BufferedReader(new FileReader(new File("obiekty\\" + nr + ".txt")))); 
        }
        while(!odczyt.nextLine().equals("NAZWA:"));
        nazwa = odczyt.nextLine();
        while(!odczyt.nextLine().equals("OPIS:"));
        opis = odczyt.nextLine();
        while(!odczyt.nextLine().equals("EFEKT:"));
        String ef = odczyt.nextLine();
        if(ef.equals("null"))
        {
            efekt = new Efekt();
            uzyte = true;
        }
        else
        {
            efekt = new Efekt(Integer.valueOf(odczyt.nextLine()),Integer.valueOf(odczyt.nextLine()),Integer.valueOf(odczyt.nextLine()));
            uzyte = false;
        }
        odczyt.close();
    }
    
    /**
     *
     */
    public Mebel()
    {
        numer = -1;
    }
    
    /**
     *
     * @return
     */
    public String podajNazwe()
    {
        return nazwa;
    }
    
    /**
     *
     * @return
     */
    public int podajNumer()
    {
        return numer;
    }
    
    /**
     *
     * @return
     */
    public String podajOpis()
    {
        return opis;
    }
    
    
    /**
     *
     * @param g
     * @return
     */
    @Override
    public String uzyj(OsobaGracz g)
    {
        if(efekt.jakiEfekt() == -1)
        {
            return "Tego obiektu nie da sie uzyc";
        }
        g.nalozEfekt(efekt);
        return "Uzyles " + nazwa;
    }
}
