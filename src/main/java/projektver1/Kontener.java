/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projektver1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

/**
 *
 * @author Iwo Ryszkowski
 */
public class Kontener extends Mebel{
    private int poziomSkrzyni;
    private int poziomPulapki;
    private int rozdajPulapki;
    private boolean otwarta;
    private boolean rozbrojona;
    private Przedmiot loot[];
    
    /**
     *
     * @param nr
     * @throws FileNotFoundException
     */
    public Kontener(int nr) throws FileNotFoundException {
        super(nr);
        Scanner odczyt = new Scanner(new BufferedReader(new FileReader(new File("obiekty\\kontenery\\" + nr + ".txt"))));
        while(!odczyt.nextLine().equals("POZIOM:"));
        poziomSkrzyni = Integer.valueOf(odczyt.nextLine());
        while(!odczyt.nextLine().equals("ILE:"));
        int iloscPrzedmiotow = Integer.valueOf(odczyt.nextLine());
        loot = new Przedmiot[iloscPrzedmiotow];
        while(!odczyt.nextLine().equals("LOOT:"));
        String rand = odczyt.nextLine();
        if(rand.equals("TAK"))
        {
            for(int i = 0;i < iloscPrzedmiotow;i++)
            {
                String nastPrz = lootRoulette();
                int rodzaj = Integer.valueOf(nastPrz.charAt(0));
                nastPrz = nastPrz.substring(1);
                switch(rodzaj)
                {
                    case 1:
                        loot[i] = new Przedmiot(nastPrz);
                        break;
                    case 2:
                        loot[i] = new Wyposazenie(nastPrz);
                        break;
                    case 3:
                        loot[i] = new Bron(nastPrz);
                        break;
                    
                }
            }
        }
        else
        {
            for(int i = 0;i < iloscPrzedmiotow;i++)
            {
                String nastPrz = odczyt.nextLine();
                int rodzaj = Integer.valueOf(nastPrz.substring(0, 1));
                nastPrz = nastPrz.substring(1);
                switch(rodzaj)
                {
                    case 1:
                        loot[i] = new Przedmiot(nastPrz);
                        break;
                    case 2:
                        loot[i] = new Wyposazenie("wyposazenie\\\\" + nastPrz);
                        break;
                    case 3:
                        loot[i] = new Bron("wyposazenie\\\\bron\\\\" + nastPrz);
                        break;
                    
                }
            }
        }
        while(!odczyt.nextLine().equals("PULAPKA:"));
        if(poziomSkrzyni == 0)
        {
            poziomPulapki = 0;
        }
        else
        {
            poziomPulapki = Testy.test2(poziomSkrzyni);
        }
        if(poziomPulapki == 0)
        {
            rozbrojona = true;
            rozdajPulapki = -1;
        }
        else
        {
            rozbrojona = false;
            rozdajPulapki = Integer.valueOf(odczyt.nextLine());
        }
        otwarta = true;
    }

    /**
     *
     */
    public Kontener()
    {
        super();
    }
    private String lootRoulette() throws FileNotFoundException
    {
        int r = Testy.test2(100) + poziomSkrzyni*100;
        Scanner odczyt = new Scanner(new BufferedReader(new FileReader(new File("loot.txt"))));
        for(int i = 1;i < r;i++)
        {
            odczyt.nextLine();
        }
        return odczyt.nextLine().replaceAll(Integer.toString(r), "");
    }
    
    /**
     *
     * @param nazwa
     * @return
     */
    public String dajOpisPrzedmiotu(String nazwa)
    {
        int ile = 0;
        int gdzie = 0;
        for(int i = 0; i < loot.length;i++)
        {
            if(loot[i].dajNazwe().equals(nazwa))
            {
                return loot[gdzie].dajOpis();
            }
        }
        return "Nie widzisz takiego przedmiotu";
    }
    
    /**
     *
     * @return
     */
    @Override
    public String podajOpis()
    {
        
        if(otwarta == false)
        {
            return super.podajOpis();
        }
        else
        {
            String opis2 = "W skrzyni widzisz:";
            int it = 0;
            for(int i = 0; i < loot.length;i++)
            {
                if(loot[i].numer != -1)
                {
                    it++;
                    opis2 += "\n" + it + "." + loot[i].dajNazwe();
                }
            }
            if(it == 0)
            {
                opis2 = "Skrzynia jest pusta";
            }
        return opis2;
        }
    }
    
    /**
     *
     * @return
     */
    public boolean czyRozbrojona()
    {
        return rozbrojona;
    }
    
    /**
     *
     * @return
     */
    public boolean czyOtwarta()
    {
        return otwarta;
    }
    
    /**
     *
     * @param g
     * @return
     */
    private int rozbroj(OsobaGracz g)
    {
        if(rozbrojona == true)
        {
            return 1;
        }
        int sukcesy = Testy.test1(g.zrecznosc[1], 0, g.szczescie);
        rozbrojona = true;
        if(sukcesy >= poziomPulapki)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
    /**
     *
     * @return
     */
    public int otworz()
    {
        if(otwarta == true)
        {
            return -1;
        }
        otwarta = true;
        if(rozbrojona == true)
        {
            return 1;
        }
        else
        {
            rozbrojona = true;
            return 0;
        }
    }
    
    /**
     *
     * @param g
     * @return
     */
    public String dzialaniePulapki(OsobaGracz g)
    {
        switch(rozdajPulapki)
        {
            case 1:
                g.punktyZycia[1] -= poziomPulapki;
                return "Ukryte ostrze zadaje ci " + Integer.toString(poziomPulapki) + " obrazen";
            case 2:
                for(int i = 0;i < loot.length;i++)
                {
                    if(Testy.test2(poziomPulapki) > 0)
                    {
                        loot[i] = new Przedmiot();
                    }
                }
                return "Ukryta fiolka kwasu rozbija sie i niszzczy czesc przedmiotow";
            default:
                return "Nic sie nie stalo"; 
        }
    }
    
    /**
     *
     * @param g
     * @return
     */
    public int wykryjPulapke(OsobaGracz g)
    {
        int test = Testy.test1(g.inteligencja[1], 0, g.szczescie);
        if(test == 0)
        {
            return -1;
        }
        else if(test == 1)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }
    
    /**
     *
     * @return
     */
    public int wielkosc()
    {
        return loot.length;
    }
    
    /**
     *
     * @param nazwaP
     * @return
     */
    public Przedmiot znajdzPrzedmiot(String nazwaP)
    {
        for(int i = 0;i < loot.length;i++)
        {
            if(loot[i].dajNazwe().equals(nazwaP))
            {
                return loot[i];
            }
        }
        return new Przedmiot();
    }
    
    /**
     *
     * @param nazwaP
     * @return
     */
    public boolean usunPrzedmiot(String nazwaP)
    {
        for(int i = 0;i < loot.length;i++)
        {
            if(loot[i].dajNazwe().equals(nazwaP))
            {
                loot[i] = new Przedmiot();
                return true;
            }
        }
        return false;
    }
    
    /**
     *
     * @param g
     * @return
     */
    public boolean probaOtwarcia(OsobaGracz g)
    {
        if(otwarta == true)
        {
            return false;
        }
        
        if(rozbroj(g) == 0)
        {
            dzialaniePulapki(g);
        }
        otworz();
        return true;
    }
    
    /**
     *
     * @param g
     * @return
     */
    @Override
    public String uzyj(OsobaGracz g)
    {
        return "Tego obiektu nie da sie uzyc";
    }
    
    /**
     *
     * @return
     */
    
}
