/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projektver1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

/**
 *
 * @author Iwo Ryszkowski
 */
public class Przedmiot implements Uzywalne{
    int numer;
    String nazwa;
    String opis;
    Efekt efekty[];
    int waga;
    
    /**
     *
     * @param plik
     * @throws FileNotFoundException
     */
    public Przedmiot(String plik) throws FileNotFoundException
    {
        String sciezka = "przedmioty\\" + plik + ".txt";
        Scanner odczyt = new Scanner(new BufferedReader(new FileReader(sciezka)));
        while(!odczyt.nextLine().equals("NUMER:"));
        numer = Integer.valueOf(odczyt.nextLine());
        while(!odczyt.nextLine().equals("NAZWA:"));
        nazwa = odczyt.nextLine();
        while(!odczyt.nextLine().equals("OPIS:"));
        opis = odczyt.nextLine();
        while(!odczyt.nextLine().equals("WAGA:"));
        waga = Integer.valueOf(odczyt.nextLine());
        while(!odczyt.nextLine().equals("EFEKTY:"));
        int ile = Integer.valueOf(odczyt.nextLine());
        if(ile == 0)
        {
            efekty = new Efekt[1];
            efekty[0] = new Efekt();
        }
        else
        {
            efekty = new Efekt[ile];
            for(int i = 0; i < ile;i++)
            {
                efekty[i] = new Efekt(Integer.valueOf(odczyt.nextLine()), Integer.valueOf(odczyt.nextLine()), Integer.valueOf(odczyt.nextLine()));
            }
        }
        odczyt.close();
        
    }

    /**
     *
     */
    public Przedmiot()
    {
        numer = -1;
        nazwa = "";
        opis = "";
        waga = 0;
    }
    
    /**
     *
     * @return
     */
    public String dajOpis()
    {
        return opis;
    }
    
    /**
     *
     * @return
     */
    public String dajNazwe()
    {
        return nazwa;
    }
    
    /**
     *
     * @param g
     * @return
     */
    @Override
    public String uzyj(OsobaGracz g)
    {
        String efekt = "";
        for(int i = 0;i < efekty.length;i++)
        {
            g.nalozEfekt(efekty[i]);
        }
        return "";
    }
    
    
}
