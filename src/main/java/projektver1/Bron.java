/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projektver1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

/**
 *
 * @author Iwo Ryszkowski
 */
public class Bron extends Wyposazenie{
    boolean magiczna;
    int obrazenia;
    Atak ataki[];
    
    /**
     *
     * @param plik
     * @throws FileNotFoundException
     */
    public Bron(String plik) throws FileNotFoundException
    {
        super(plik);
        Scanner odczyt = new Scanner(new BufferedReader(new FileReader("przedmioty\\" + plik + ".txt")));
        while(!odczyt.nextLine().equals("MAGIA:"));
        if(odczyt.nextLine().equals("1"))
        {
            magiczna = true;
        }
        else
        {
            magiczna = false;
        }
        while(!odczyt.nextLine().equals("OBRAZENIA:"));
        obrazenia = Integer.valueOf(odczyt.nextLine());
        while(!odczyt.nextLine().equals("ATAKI:"));
        ataki = new Atak[Integer.valueOf(odczyt.nextLine())];
        for(int i = 0;i < ataki.length;i++)
        {
            ataki[i] = new Atak(odczyt.nextLine());
        }
        odczyt.close();
    }
    
    /**
     *
     */
    public Bron()
    {
        super();
        magiczna = false;
        obrazenia = 0;
        ataki = new Atak[1];
        ataki[0] = new Atak();
    }
    
    /**
     *
     * @return
     */
    public String opiszAtaki()
    {
        String opis ="";
        for(int i = 0;i < ataki.length;i++)
        {
            opis += (i+1) + ". " + ataki[i].opisAtaku() + "\n";
        }
        return opis;
    }
}
