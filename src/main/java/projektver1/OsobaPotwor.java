/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projektver1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

/**
 *
 * @author Iwo Ryszkowski
 */
public class OsobaPotwor extends Osoba{
    Efekt tablicaEfektow[];
    OsobaPotwor(String plik,int numer,int polozenie) throws FileNotFoundException
    {
        super(plik,numer,polozenie);
        Scanner odczyt;
        odczyt = new Scanner(new BufferedReader(new FileReader("osoby\\" + plik + ".txt")));
        while(!odczyt.nextLine().equals("EFEKTY:"));
        int ile = odczyt.nextInt();
        if(ile == 0)
        {
            tablicaEfektow = new Efekt[1];
            tablicaEfektow[0] = new Efekt();
        }
        else
        {
            tablicaEfektow = new Efekt[ile];
            int j,i,t;
            for(int k = 0;k < ile;k++)
            {
                j = odczyt.nextInt();
                i = odczyt.nextInt();
                t = odczyt.nextInt();
                tablicaEfektow[k] = new Efekt(j,i,t);
            }
        }
        odczyt.close();
    }
    
    OsobaPotwor()
    {
        super();
        tablicaEfektow = new Efekt[1];
        tablicaEfektow[0] = new Efekt();
    }
    
    /**
     *
     * @return
     */
    public Efekt losujEfekt()
    {
        if(tablicaEfektow[0].jakiEfekt() != -1)
        {
            int czy = Testy.test2(10);
            if(czy <= 7)
            {
                return new Efekt();
            }
            czy = Testy.test2(tablicaEfektow.length);
            return tablicaEfektow[czy];
        }
        return new Efekt();
    }
}
